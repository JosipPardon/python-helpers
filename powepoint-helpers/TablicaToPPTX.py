# ATTENTNION: PPTX PACKAGE RUNS ONLY ON CERTAINS VERSION OF PYTHON (https://python-pptx.readthedocs.io/en/latest/user/install.html)

from pptx import Presentation
from pptx.util import Pt
from pptx.enum.text import PP_ALIGN
import copy
import os
from openpyxl import load_workbook

DIR_PATH = os.path.dirname(os.path.realpath(__file__))

#modeled on https://stackoverflow.com/a/56074651/20159015 and https://stackoverflow.com/a/62921848/20159015
#this for some reason doesnt copy text properties (font size, alignment etc.)
def SlideCopyFromPasteInto(copyFromPres, slideIndex,  pasteIntoPres):

    # specify the slide you want to copy the contents from
    slide_to_copy = copyFromPres.slides[slideIndex]

    # Define the layout you want to use from your generated pptx

    slide_layout = pasteIntoPres.slide_layouts.get_by_name("Blank") # names of layouts can be found here under step 3: https://www.geeksforgeeks.org/how-to-change-slide-layout-in-ms-powerpoint/
    # it is important for slide_layout to be blank since you dont want these "Write your title here" or something like that textboxes
    # alternative: slide_layout = pasteIntoPres.slide_layouts[copyFromPres.slide_layouts.index(slide_to_copy.slide_layout)]
    
    # create now slide, to copy contents to 
    new_slide = pasteIntoPres.slides.add_slide(slide_layout)

    # create images dict
    imgDict = {}

    # now copy contents from external slide, but do not copy slide properties
    # e.g. slide layouts, etc., because these would produce errors, as diplicate
    # entries might be generated

    
    for shp in slide_to_copy.shapes:
        if 'Picture' in shp.name:
            # save image
            with open(shp.name+'.jpg', 'wb') as f:
                f.write(shp.image.blob)

            # add image to dict
            imgDict[shp.name+'.jpg'] = [shp.left, shp.top, shp.width, shp.height]
    
    # things added first will be covered by things added last => since I want pictures to be in background, I will add them first

    # add pictures
    for k, v in imgDict.items():
        new_slide.shapes.add_picture(k, v[0], v[1], v[2], v[3])
        os.remove(k)

    for shp in slide_to_copy.shapes:
        if not('Picture' in shp.name):
            # create copy of elem
            el = shp.element
            newel = copy.deepcopy(el)

            # add elem to shape tree
            new_slide.shapes._spTree.insert_element_before(newel, 'p:extLst')

    return new_slide # this returns slide so you can instantly work with it when it is pasted in presentation



# 
quizType = 1 #input("Unesi 0 za regularni kviz, 1 za interaktivno hiperaktivni (i stisni enter): ")

worksheetName = ""
templateSlideIndex = 0
if (quizType == 0):
    worksheetName = 'regularni'
    templateSlideIndex = 0
else:
    worksheetName = 'interaktivno-hiperaktivni'
    templateSlideIndex = 1

templatePres = Presentation(f"{DIR_PATH}/template.pptx")
outputPres = Presentation() 
outputPres.slide_height, outputPres.slide_width = templatePres.slide_height, templatePres.slide_width
# this can sometimes cause problems. Alternative:
# outputPres = Presentation(f"{DIR_PATH}/template.pptx") and now delete all slides to have empty presentation

workbook = load_workbook(f"{DIR_PATH}/tablica.xlsx", data_only=True)
print(workbook.sheetnames)
worksheet = workbook.get_sheet_by_name(worksheetName)

rowIterator = 3 #jer sve pocinje u 3 retku
while True:
    row = {
        "redniBroj": worksheet[f'B{rowIterator}'].value, 
        "imeEkipe": worksheet[f'C{rowIterator}'].value, 
        "brojBodova": worksheet[f'D{rowIterator}'].value,
    }
    if row["redniBroj"] == None: break
    rowIterator += 1

    pastedSlide = SlideCopyFromPasteInto(templatePres,templateSlideIndex,outputPres)


    for shape in pastedSlide.shapes:

        if not(shape.has_text_frame): continue

        
        if shape.text_frame.text == "redniBroj": 
            shape.text_frame.text = f'{row["redniBroj"]}. mjesto'

        if shape.text_frame.text == "imeEkipe":
            shape.text_frame.text = f'{row["imeEkipe"]}'

        # stylizing text need to be done after you change it
        shape.text_frame.paragraphs[0].font.size = Pt(80) 
        shape.text_frame.paragraphs[0].alignment = PP_ALIGN.CENTER
        


outputPres.save(f'{DIR_PATH}/output.pptx')
