"""
INSTRUCTIONS:




"""
"""
uzor: https://www.geeksforgeeks.org/convert-pdf-to-image-using-python/
"""

# import module 
from pdf2image import convert_from_path
import sys
import os
 
 
# Store Pdf with convert_from_path function
images = convert_from_path("test")
for i in range(len(images)):
    
    # Save pages as images in the pdf
    images[i].save('page'+ str(i) +'.png', 'PNG')

    # treba neki poppler instalirat https://pypi.org/project/pdf2image/
    